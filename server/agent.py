import grpc
import miniproject3_pb2
import miniproject3_pb2_grpc
from concurrent import futures
import time, os, threading, pika
import random, string, secrets
import requests
from werkzeug.utils import secure_filename


def generate_routing_key(length):
    return ''.join(secrets.choice(string.ascii_letters + string.digits) for x in range(length))


def publish_progress(percentage, routing_key, exchange_name='1806205653'):
    print(percentage, flush=True)
    connection = pika.BlockingConnection(
        pika.URLParameters('amqp://guest:guest@rabbitmq')
    )
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange_name, exchange_type="direct")
    channel.basic_publish(
        exchange=exchange_name,
        routing_key=routing_key,
        body=bytes(str(percentage), encoding="UTF-8"),
        mandatory=True,
    )


def download(url, file_path, filename, routing_key, exchange_name):
    with open(file_path, "wb") as f:
        print("Downloading %s" % file_path, flush=True)
        response = requests.get(url, stream=True)
        total_length = response.headers.get('content-length')
        print(total_length, flush=True)

        if total_length is None: # no content length header
            f.write(response.content)
        else:
            dl = 0
            total_length = float(total_length)
            for data in response.iter_content(chunk_size=1024*1024):
                dl += len(data)
                f.write(data)
                done = round((dl / total_length) * 100,2)
                publish_progress(done, routing_key, exchange_name)        
        publish_progress('http://localhost:5000/' + file_path, routing_key, exchange_name)


def async_download(url, routing_key, exchange_name='1806205653'):
    filename = url.split("/")[-1]
    file_path = os.path.join('downloads', '', filename)
    thread = threading.Thread(target=download,args=(url, file_path, filename, routing_key, exchange_name))
    thread.start()


class MiniProjectServicer(miniproject3_pb2_grpc.MiniProjectServiceServicer):
    def Download(self, request, context):
        url = request.url
        uniq_id = generate_routing_key(20)
        async_download(url, uniq_id)

        response = miniproject3_pb2.DownloadResponse()
        response.url = url
        response.uniq_id = uniq_id

        return response


def main():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    miniproject3_pb2_grpc.add_MiniProjectServiceServicer_to_server(
        MiniProjectServicer(), server)

    print('Starting server. Listening on port 50051.')
    server.add_insecure_port('[::]:50051')
    server.start()

    try:
        while True:
            time.sleep(86400)

    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    main()