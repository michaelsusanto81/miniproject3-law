from flask import Flask, render_template, request, send_from_directory
from werkzeug.utils import secure_filename
import os
import requests
import grpc
import miniproject3_pb2 as pb
import miniproject3_pb2_grpc

app = Flask(__name__)

# create the folders when setting up your app
os.makedirs('downloads', exist_ok=True)

def call_with_grpc(data):
    # Create channel and stub to server's address and port.
    channel = grpc.insecure_channel('agent:50051')
    stub = miniproject3_pb2_grpc.MiniProjectServiceStub(channel)
    response = ''

    # Exception handling.
    try:
        response = stub.Download(pb.DownloadRequest(url=data['url']))

    # Catch any raised errors by grpc.
    except grpc.RpcError as e:
        response = "Error raised: " + e.details()

    return response

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        grpc_response = call_with_grpc(request.form)
        response = {
            'url': grpc_response.url,
            'routing_key': grpc_response.uniq_id,
            'exchange_name': '1806205653'
        }

        return render_template('index.html', **response)
    return render_template('index.html')

@app.route('/downloads/<filename>')
def upload(filename):
    return send_from_directory('downloads', filename, as_attachment=True)

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
