# Mini Project 3 - LAW
Remote Download - Download & Asynchronous Progress Status
Using Message Queue

## Author
Michael Susanto - 1806205653

## Backup Repository
https://gitlab.com/michaelsusanto81/miniproject3-law

## Techstacks
- **RabbitMQ**
- **Flask (Web Server)**
- **gRPC Server (Background Download Agent)**

## How to use in Local
- Clone the repository
```cmd
git clone https://gitlab.com/michaelsusanto81/miniproject3-law
```

- Run the app
```cmd
docker-compose up -d --build
```

- Open **http://localhost:5000**

- Stop the app
```cmd
docker-compose down -v
```
